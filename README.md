# Nástroj na vyplňování online testů 
Cílem je vytvořit nástroj pro vyplňování online testů, u kterých je zajištěno, že dokončení nelze jednoduchým způsobem podvrhnout. Nástroj musí být navržen v ASP .Net Core 5.0 s využitím Azure Table jako vrstvy pro ukládání dat.

## Vizuální stránka aplikace

Nástroj má dvě vstupní stránky /manage a /test. Vizuální rozvržení stránek je čistě na vaší kreativitě. Responsibilita pro mobilní aplikace není nutná.

### Stránka manage

Na stránce manage jsou dostupná následující pole

    Jméno a příjmení
    Email
    Alternativní email pro zaslání výsledku

    [Vygenerovat odkaz pro zahájení testu]

Po stisku tlačítka se vygeneruje odkaz na veřejnou adresu public-url/test?id=GUID a tento odkaz se zobrazí na stránce (místo polí pro vyplnění údajů). Pro opětovné zobrazení polí pro vyplnění údajů (Jméno atd.) postačí refresh stránky.

Identifikace testu, společně s registračními údaji, se uloží do Azure Table Storage. Pro přístup k tabulce využijte SAS token s Url, který bude definován v konfiguračním souboru.
Jako partition key se použije GUID z nastavení aplikace (``TestPartitionKey``) a jako row key se použije GUID vygenerovaného testu. Do řádku se uloží další informace získané při registraci testu (``FullName``, ``Email``, ``AlternativeEmail``)

## Stránka testu

Při spuštění testu se zobrazí uvítání s napevno definovaným textem ve stránce

    Dobrý den, víteje v testu "název testu z konfigurace". Na každou otázku je pevně daný časový limit a test není možné opakovat. Jeho vyplnění je tedy vhodné na stabilním internetovém připojení. Pokud chcete test mezi otázkami pozastavit, vyberte správnou odpověď a neprovádějte stisk tlačítka pokračovat. Celkový limit na vyplnění testu (včetně případných přestávek) je "spočtený celkový čas".

    [Zahájit test]

Každá otázka testu je na samostatné stránce. Na stránce musí být vždy vidět "Název testu" (dle nastavení aplikace) "Jméno", "Příjmení", "Celkový zbývající čas" a "Zbývající čas pro aktuální otázku". Dále pak text otázky a informace o typu otázky. Očíslované odpovědi a tlačítko "Pokračovat".

Informace o typu otázky bude text "Vyberte jednu správnou odpověď" a ve druhém případě "Zaškrtněte veškeré správné odpovědi". Tento text musí být doprovodný a vizuálním dojmem musí být neobtěžující.

Pokud dojde k vypršení stanoveného limitu, tak se zobrazí informace "Vypršel časový limit. Možnost změnit odpověď na aktuální otázku je uzavřena". Po vypršení limitu také dojde k zamknutí ovládacích prvků a bude povoleno pouze tlačítko pokračovat.

Po stisku pokračovat nebo při vypršení časového limitu se musí provést dotaz na server a provést změnu aktuální stavu. Aktuální otázka se tedy může nacházet ve dvou stavech "Dokončena" či "Probíhající". Při zahájení se uloží čas ve kterém se otázka na straně serveru zahájila. Na klientskou stranu se vrátí zbývající počet sekund do skončení testu od doby zahájení (v případě stavu dokončeno se vrátí 0). Při obnovení stránky se tedy vrátí pouze zbývající čas, stejně tak při pokusu otevření na jiném prohlížeči. Pokud klientovi nefunguje internet, tak má bohužel smůlu a problematickou otázku nestihne dokončit (nicméně další se sama automaticky nezahájí).

Informace o testu bude uložena pouze v paměti procesu a to v paměťové struktuře zajištující bezpečný vícevláknový provoz. Pro každý probíhající test je v paměti uložen jeho aktuální stav. Do Azure table se zapisuje výsledek až po poslední otázce a nebo po vypršení časového limitu na zpracování celého testu. Vypršením časového limitu se tedy test ukončí a otázky se berou jako za nezodpovězené.

Po stisku "Pokračovat" u poslední otázky, se zobrazí poděkování

    Děkujeme za vyplnění testu. Shrnutí testu bylo zasláno na váš email.

Po dokončení testu dojde také k poslání potvrzujícího emailu na adresy definované při vytvoření testu a na adresy pevně definované v konfiguračním souboru. Adresy mohou být odděleny středníkem. Pokud nedojde k dokončení testu do celkového časového limitu tak se test automaticky ukončí. Časový limit celého testu je dán 4 násobkem limitu všech otázek zakrouhleného nahoru na půlhodiny. Potvrzující email zaslaný na emailové adresy zadané v registraci bude obsahovat pouze souhrnou informaci

    Test "název testu z konfigurace" pro osobu "Jméno a Příjmení" by dokončen s úspěšností 75 %

Informace odeslaná na adresy uvedené v konfiguraci bude detailní a bude obsahovat výsledek každého testu. Přesné formátování obou emailů je na vašem uvážení.

Do Azure Table Storage se zapíše výsledek testu do několika polí. První textové pole ``DetailedResults`` bude obsahovat informace o úspěšnosti jednotlivých otázek ve formátu JSON. Jeho struktura je na vašem uvážení. Druhé pole ``Result`` bude obsahovat celkový procentuální výsledek zaokrouhlený na celá čísla (Int32). Například 75 ve smyslu 75% úspěsných otázek.  

Při startu aplikace je nutné udělat kontrolu na jednoznačnost identifikátorů otázek a v případě problému aplikaci nespouštět a vyhodit chybu.  Aplikace musí obsahovat healthcheck (Basic health probe) pro snadné monitorování v kubernetes https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/health-checks?view=aspnetcore-5.0

## appsettings.json

Budou obsahovat novou skupinu nastavení pojmenovanou ``AppSettings``, tato sekce se bude načítat pomocí options paternu (`IOptions<TOptions>`) 

Sekce ``AppSettings`` v souboru ``appsettings.json`` bude vypadat dle následujícího příkladu. Položka ``CorrectAnswer`` je samozřejmě nepovinná a ve výchozím stavu je rovna false.

    {
      "AppSettings": {
        "TestPartitionKey": "<insert-guid-here>",
        "ResultEmails" : "email1;email2"
        "TableUrl": "<insert-url-with-sas-token>"
        TestName" : "Test znalostí vývojáře ASP .Net Core"
        "Questions" :
        [
          {
            "QuestionId" : "Test01"
            "QuestionType" : "SingleChoice/MultiChoice"
            "QuestionText" : "K čemu slouží entity framework?"
            "TimeLimit" : "hh:mm:ss"
            "Answers" :
            [
              [
                {
                  "CorrectAnswer" : false,
                  "AnswerText" : "Jedná se objektovou databázi entit"
                },
                {                
                  "CorrectAnswer" : false,
                  "AnswerText" : "Jadná se o in-memory databázi objektů v .NET"
                },
                {               
                  "CorrectAnswer" : true,
                  "AnswerText" : "Jedná se o objektově relační mapování"
                }                                
              ]
            ] 
          }
        ]
      },
   